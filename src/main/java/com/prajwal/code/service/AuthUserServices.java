package com.prajwal.code.service;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.prajwal.code.Repository.UserRepository;
import com.prajwal.code.Repository.UserRoleRepository;
import com.prajwal.code.entity.User;
import com.prajwal.code.entity.UserRole;

/**
 *
 * @author Dipesh
 */
@Service
public class AuthUserServices implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user==null){
            throw new UsernameNotFoundException("User donot exist..");
        }
        UserDetails userDetails =new org.springframework.security.core.userdetails
                .User(username, user.getPassword(), getAuthorities(username));
        return userDetails;
    }
    private List<GrantedAuthority> getAuthorities(String username){
        List<GrantedAuthority> authorities = 
                new ArrayList<>();
        for(UserRole role:userRoleRepository.findByUserUsername(username)){
            authorities.add(new SimpleGrantedAuthority(role.getRole().getRoleName()));
        }
        
        return authorities;
    }
}
