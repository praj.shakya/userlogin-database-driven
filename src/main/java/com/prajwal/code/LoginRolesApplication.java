package com.prajwal.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginRolesApplication {

	public static void main(String[] args) {
		
		
		SpringApplication.run(LoginRolesApplication.class, args);
	}

}
