package com.prajwal.code.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prajwal.code.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
