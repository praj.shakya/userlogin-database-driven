package com.prajwal.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prajwal.code.Repository.RoleRepository;
import com.prajwal.code.entity.Role;

@Controller
@RequestMapping(value = "/roles")
public class RoleController {
	@Autowired
	protected RoleRepository roleRepository;
	@GetMapping
	@ResponseBody
	public List<Role> index(){
		return roleRepository.findAll();
	}
}
