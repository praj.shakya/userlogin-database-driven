package com.prajwal.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prajwal.code.Repository.UserRepository;
import com.prajwal.code.entity.User;

@Controller
@RequestMapping(value = "/users")
public class UserController {
	@Autowired
	protected UserRepository userRepository;
	
	@GetMapping
	@ResponseBody
	public List<User> index(){
		return userRepository.findAll();
	}
	
}
