package com.prajwal.code.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.prajwal.code.Repository.UserRepository;
import com.prajwal.code.entity.User;


@Controller
@RequestMapping(value = "/register")
public class RegisterController {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @GetMapping
    public String register(){
        return "/register/index";
    }
    
//    @PostMapping
//    public String save(User user){
//        user.setUsername(userRepository.findByUsername(username));
//        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//        user.setEmail(userRepository.);
//        userRepository.save(user);
//        return "redirect:/login?success";
//    }
    @PostMapping
    public String save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return "redirect:/login?success";
    }
}
