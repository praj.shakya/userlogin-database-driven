package com.prajwal.code.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping (value = "/login")
public class LoginController {
    
    @GetMapping
    public String index(Model model){
        model.addAttribute("title", "Login");
        return "/login/index";
    }
}
